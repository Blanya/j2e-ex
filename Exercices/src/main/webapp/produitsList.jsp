<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: x_mar
  Date: 07/12/2022
  Time: 11:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Liste des produits"</title>
</head>
<body>
    <%--nav--%>
    <jsp:include page="menu.jsp"></jsp:include>

<main>
    <section>
        <article class="container w-75">
            <table class="table table-info table-striped">
                <thead>
                <tr>
                    <th> Marque </th>
                    <th> Stock </th>
                    <th> En savoir plus? </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${produits}" var="produit">
                    <tr>
                        <td>${produit.getMarque()}</td>
                        <td>${produit.getStock()}</td>
                        <td><a class="text-dark" href="?id=${produit.getId()}">Afficher les détails</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </article>
        <article>
            <h2>Ajouter un produit dans la liste</h2>
            <form method="post" action="produits" enctype="multipart/form-data">
                <label for="marque">Marque: </label>
                <input type="text" name="marque" id="marque"/>

                <label for="reference">Reference: </label>
                <input type="text" name="reference" id="reference"/>

                <label for="prix">Prix: </label>
                <input type="number" name="prix" id="prix" step=".01"/>

                <label for="dateAchat">Date d'achat: </label>
                <input type="date" name="dateAchat" id="dateAchat"/>

                <label for="stock">Stock: </label>
                <input type="number" name="stock" id="stock"/>

                <input type="file" name="image" id="image">

                <button type="submit">Ajouter un produit</button>
            </form>
        </article>
    </section>
</main>

</body>
</html>
