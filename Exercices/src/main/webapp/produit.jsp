<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: x_mar
  Date: 07/12/2022
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Détails d'un produit</title>
</head>
<body>
    <jsp:include page="menu.jsp"></jsp:include>
    <ul>
        <li>${produit.getMarque()}</li>
        <li>${produit.getReference()}</li>
        <li>${produit.getPrix()}</li>
        <li>${produit.getDateAchat()}</li>
        <li>${produit.getStock()}</li>
    </ul>

    <c:if test="${(produit.getImages().size()>0)}">
        <c:forEach items="${produit.getImages()}" var="image">
            <img src="${image.getUrl()}">
        </c:forEach>
    </c:if>
    


<%--    <form method="post" action="produits/details" enctype="multipart/form-data">--%>
<%--        <div>--%>
<%--            <input type="file" name="image" id="image">--%>
<%--        </div>--%>
<%--        <div>--%>
<%--            <button type="submit">Envoyer</button>--%>
<%--        </div>--%>
<%--    </form>--%>

    <a href="produits">
        <button>Retour aux produits</button>
    </a>
</body>
</html>
