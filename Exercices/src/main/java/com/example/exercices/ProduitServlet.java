package com.example.exercices;

import com.example.exercices.models.Image;
import com.example.exercices.models.Produit;
import com.example.exercices.services.ImageService;
import com.example.exercices.services.ProduitService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(name = "produitServlet", value = "/produits")
@MultipartConfig(
        maxFileSize = 1024 * 1024 * 10, //10 MB
        maxRequestSize = 1024 * 1024 * 100 //100 MB
)
public class ProduitServlet extends HttpServlet {

    private List<Produit> produits = new ArrayList<>();
    private ProduitService produitService;
    private ImageService imageService;

    public void init(){
        produitService = new ProduitService();
        imageService = new ImageService();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        produits = produitService.findAll();

        //Si parameter => afficher un produit
        if(request.getParameter("id") != null)
        {
            int id = Integer.parseInt(request.getParameter("id"));
            Produit produit = new Produit();
            try{
                produit = produitService.findById(id);
            }catch (Exception e){

            }
            if(produit != null)
            {
               request.setAttribute("produit", produit);
               request.getRequestDispatcher("produit.jsp").forward(request, response);
            }
        }
        //Sinon afficher la liste
        else {
                request.setAttribute("produits", produits);
                request.getRequestDispatcher("produitsList.jsp").forward(request, response);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if(!request.getParameter("marque").isBlank() && !request.getParameter("prix").isBlank() && !request.getParameter("reference").isBlank() && !request.getParameter("stock").isBlank() && !request.getParameter("dateAchat").isBlank())
        {
            String marque = request.getParameter("marque");
            String reference = request.getParameter("reference");
            double prix = Double.parseDouble(request.getParameter("prix"));
            int stock = Integer.parseInt(request.getParameter("stock"));
            Date date;
            try {
                date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("dateAchat"));
            } catch (ParseException e) {
                //par défaut
                date = new Date();
            }

            Produit p = new Produit(marque, reference, date, prix, stock);

            if(!request.getParts().isEmpty()){
                //recupere champ type file
                Part filePart = request.getPart("image");
                String fileName = filePart.getSubmittedFileName();
                String uploadPath = getServletContext().getRealPath("/")+"images";
                File uploadDir = new File(uploadPath);

                //Création du répertoire s'il n'existe pas ds l'emplacement du deploiement de l'app ds tomcat
                if(!uploadDir.exists()){
                    uploadDir.mkdir();
                }

                for(Part part: request.getParts()){
                    part.write(uploadPath+File.separator+fileName);
                }

                //Enregistrer image
                Image image = new Image("images"+File.separator+fileName);
                produitService.create(p);

                p.addImage(image);
                imageService.create(image);
                produitService.update(p);
            }
            if(produitService.create(p))
            {
                produits = produitService.findAll();
                request.setAttribute("produits", produits);
                request.getRequestDispatcher("produitsList.jsp").forward(request, response);
            }
            else {
                //mettre page erreur
            }
        }
        else {
            //mettre page erreur
        }

    }
}
