package com.example.exercices.services;

import com.example.exercices.dao.IDao;
import com.example.exercices.models.Produit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.List;

public class ProduitService implements IDao<Produit> {

    static StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    static SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    public static Session session = sessionFactory.openSession();

    @Override
    public boolean create(Produit o) {
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(Produit o) {
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();

            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Produit o) {
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();

            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Produit findById(int id) {
            session.beginTransaction();
            Produit produit = session.get(Produit.class, id);
            session.getTransaction().commit();

            return produit;
    }

    @Override
    public List<Produit> findAll() {
        try {
            List<Produit> produits = null;

            session.beginTransaction();
            Query<Produit> listQuery = ProduitService.session.createQuery("FROM Produit");
            produits = listQuery.list();
            session.getTransaction().commit();

            return produits;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
