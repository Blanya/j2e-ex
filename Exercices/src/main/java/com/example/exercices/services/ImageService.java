package com.example.exercices.services;

import com.example.exercices.dao.IDao;
import com.example.exercices.models.Image;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.List;

public class ImageService implements IDao<Image> {
    static StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    static SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    public static Session session = sessionFactory.openSession();

    @Override
    public boolean create(Image o) {
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Image o) {
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(Image o) {
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public Image findById(int id) {
        Image image = null;

        session.beginTransaction();
        image = (Image) session.get(Image.class, id);
        session.getTransaction().commit();

        return image;
    }

    @Override
    public List<Image> findAll() {
        session.beginTransaction();
        Query<Image> imageQuery= session.createQuery("from Image");
        session.getTransaction().commit();
        return imageQuery.list();
    }

}
